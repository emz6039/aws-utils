#!/bin/bash

function uploadlambdajar(){
    declare -A profiles
    profiles[account1]="arn:aws:iam::123456789012:role/west-role"
    profiles[account2]="arn:aws:iam::987654321021:role/east-role"
    #upload lambda 
    #lambda name
    LAMBDA_NAME="foobar"
    # example arn
    #"arn:aws:lambda:us-east-2:123456789012:function:my-function:1"
    TARGET=$(find . -regex ".*\.jar")
    aws lambda update-function-code --function-name $LAMBDA_NAME --zip-file fileb://$TARGET        

}


