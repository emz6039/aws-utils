#!/bin/bash

function makeawssession(){
    declare -A profiles
    profiles[account1]="arn:aws:iam::123456789012:role/west-role"
    profiles[account2]="arn:aws:iam::987654321021:role/east-role"
    # session=$(aws sts assume-role --role-arn '${profiles[$PROFILE]}' --role-session-name AWSCLI-Session)

    # example response from https://docs.aws.amazon.com/cli/latest/reference/sts/assume-role.html#examples
    session='{
        "AssumedRoleUser": {
            "AssumedRoleId": "AROA3XFRBF535PLBIFPI4:s3-access-example",
            "Arn": "arn:aws:sts::123456789012:assumed-role/xaccounts3access/s3-access-example"
        },
        "Credentials": {
            "SecretAccessKey": "9drTJvcXLB89EXAMPLELB8923FB892xMFI",
            "SessionToken": "AQoXdzELDDY//////////wEaoAK1wvxJY12r2IrDFT2IvAzTCn3zHoZ7YNtpiQLF0MqZye/qwjzP2iEXAMPLEbw/m3hsj8VBTkPORGvr9jM5sgP+w9IZWZnU+LWhmg+a5fDi2oTGUYcdg9uexQ4mtCHIHfi4citgqZTgco40Yqr4lIlo4V2b2Dyauk0eYFNebHtYlFVgAUj+7Indz3LU0aTWk1WKIjHmmMCIoTkyYp/k7kUG7moeEYKSitwQIi6Gjn+nyzM+PtoA3685ixzv0R7i5rjQi0YE0lf1oeie3bDiNHncmzosRM6SFiPzSvp6h/32xQuZsjcypmwsPSDtTPYcs0+YN/8BRi2/IcrxSpnWEXAMPLEXSDFTAQAM6Dl9zR0tXoybnlrZIwMLlMi1Kcgo5OytwU=",
            "Expiration": "2016-03-15T00:05:07Z",
            "AccessKeyId": "ASIAJEXAMPLEXEG2JICEA"
        }
    }'
    echo "role: $profiles[$1]\n"
    secret_key=$(echo $session | jq '.Credentials.SecretAccessKey')
    session_token=$(echo $session | jq '.Credentials.SessionToken')
    access_key_id=$(echo $session | jq '.Credentials.AccessKeyId')
    echo "secret key: $secret_key"
    echo "session token: $session_token"
    echo "access key: $access_key_id" 
}



